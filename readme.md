# Deployer

Deployer is an automated script built for handling BitBucket's POST
hook deployment process.

When you put this script to a location accessible to BitBucket, you can set up
a configuration file which matches your environment, then you set up a POST hook
in your repository and everytime you push data in there, the POST hook calls
your instance of Deployer. 

It downloads head revision in a branch you want to observe, then flushes cache
in a specified directory and memcached servers. Deployment report is then sent
via SMTP server to specified inboxes.

## Requirements

 1. PHP + Apache server
 2. Read access to your Git repository using pubkey authorization
 3. Git repository already initialized (= when you enter the repo root and type
    "git pull", your repo gets updated)
 4. You need to have a functional connection between your server and BitBucket
    using custom Deployment keys
 5. Deployer does not use tags for indicating "this commit should go
    to the server" - it just pulls the head of branch defined in
    DeployConfig::$branch. You have to maintain a branch in which you merge the
    revision you want to push to your server. Deployer then checks out
    that branch and pulls whatever newest there is.

## Security

There is no authentication for POST hooks, anybody can trigger your deployment
script if you don't set up your environment correctly. It won't probably damage
your server (assuming only trusted people have write access to your repository),
but at least it would cause malicious cache flushing and emails delivery, which
is at least annoying, if not harmful to the server speed.

### You should:

 1. Make sure all .htaccess rules are working (nobody can access subdirectories
    within your Deployer directory)
 2. Extend your .htaccess to allow access only from certain IP addresses
 3. Make sure the URL which you call is at least /^[a-z0-9]{64}$/i
 4. Make sure you never insert your production config files to your source
    control software 

## Installation and usage

Following steps assume you are using a domain name of http://example.com and all
above requirements are met.

### Installing via composer

Just add ''"deployer": "dev-master",'' to your composer.json

In this case every configuration file and log file is stored inside directory
created by composer. Make sure ''tmp/'' directory is writable by both
webserver user and deployment user.

### Installing manually

Copy all files from the ''src/'' folder somewhere, where your webserver can
serve them via browser.

In this case every configuration file and log file is stored inside directory
where you placed everything. Make sure ''tmp/'' directory is writable by both
webserver user and deployment user.

### Setup with with webserver user having RW access

You can apply this use case / instructions if you can issue ''git pull'' under
the user which runs your web server (usualy www-data or nobody). That is highly
unusual for a production environment and if you are running such server, keep in
mind that if any attacket gains any significant level of access to your web
server, he could alter files you have there and cause you trouble. 

But - whatever works. If that's your case, go on. If not, skip to the next point.

Here you go:

 1. Make sure your files are accessible through your browser
    (say http://example.com/deployer)
 2. Copy the "config/config.sample.php" to a configuration file named
    ''config/config.SOMESECRETSTRING.php'' with the "SOMESECRETSTRING" being
    any word of alphanumeric characters between 16 and 128 characters long
    (say "config/config.xiRrubWvkXGz8uNi.php").
 3. Make sure you set your configuration file according to your needs (see the
    sample file for more documentation)
 4. Open http://example.com/deployer/xiRrubWvkXGz8uNi.php and you should see
    a deployment report - ending with "Deyploment finished.". You should also
    receive an email report at the address you set to your config file.
 5. Go to https://bitbucket.org/YOURREPOSITORY/admin/hooks and set add
    your POST hook URL to http://example.com/deployer/xiRrubWvkXGz8uNi.php
 6. Make sure you push some data to a branch which you set into
    DeployConfig::$gitBranch
 7. Push data to your repository.
 8. Sit back, relax and enjoy waiting for the deployment report.

### Setup with with webserver user having Read only access

This use case applies to all situations where the webserver user has only read
access to the directory structure of your website. 

That means you either can't issue any git binary commands or even if you did it,
you'd have no chance to write the updated files. 

In this case the Deployer works in two steps: 

 1. The POST request to deploy.php registers the POST request into a file in
    ''tmp/pending/*.json'' directory
 2. Any CLI call of the deploy.php checks for all the files in
    ''tmp/pending/*.json'' and executes them.

The key point is that you can run the CLI call via a different user than the 
webserver user. You just need to maintain an extra cronjob for that.

Here you go:

 1. Proceed with steps 1 through 5 in the RW use case (above)
 2. Make sure to set your DeployConfig::$requireCliExecution to true
 3. Make sure to set your DeployConfig::$requireUserForDeploy to the user which
    needs to be used for successful ''git pull'' execution
 4. Create a cron job which calls the deploy.php script as often as you wish
 5. Push data to your repository.
 6. Sit back, relax and enjoy waiting for the deployment report.


## Troubleshooting

 1. Most errors get logged into ''tmp/log/*.log'' files.
 2. Errors occurring before the configuration file is identified get logged into
    ''tmp/log/error.log'' and if there is something major, it gets logged into
    your standard error log
 3. Progress of any deployment with correct deployment job gets logged into a
    log file name matching the deploy job.
 4. If your deployment report didn't arrive, check error logs and try to call
    the deployment URL set in your POST hook manually. You will see some output.
 5. If you don't see any output, check your server's error log and check if you
    have error logging enabled.

## Potential plans for the future

 1. Add execution of unit tests, cancel deployment if any test fails
 2. Add calling of ''composer update'' at the end of deployment cycle

## Contribution

Any contribution is welcome as well as issue reporting in this BitBucket
repository. If you find this tool handy, you can buy me a beer when you're in 
Prague (Czech Republic).

Cheers!